use super::hash::{Hashable, H256};
/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    num_leaf: usize,
    tree_size: usize,
    nodes_array: Vec<H256>,
    level_num: Vec<usize>,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        //unimplemented!()
        let data_len = data.len();
        let mut v: Vec<H256> = Vec::new();
        let mut l: Vec<usize> = Vec::new();
        
        for n in 0..data_len{
            v.push(data[n].hash());
        }
        l.push(data_len);

        let mut lvl_num = data_len;
        let mut cur_pos = 0;
        let mut base = 0;
        let mut new_lvl_count = 0;
        while lvl_num > 1{
            while cur_pos < lvl_num{
                let mut raw_hash: [u8; 64] = [0; 64];
                raw_hash[0..32].copy_from_slice(v[cur_pos + base].as_ref());
                if cur_pos + 1 < lvl_num{
                    raw_hash[32..64].copy_from_slice(v[cur_pos + 1 + base].as_ref());
                } else {
                    raw_hash[32..64].copy_from_slice(v[cur_pos + base].as_ref());
                }
                v.push(ring::digest::digest(&ring::digest::SHA256, &raw_hash).into());
                cur_pos += 2;
                new_lvl_count += 1;
            }
            l.push(new_lvl_count);
            base += new_lvl_count;
            lvl_num = new_lvl_count;
            new_lvl_count = 0;
            cur_pos = 0;
        }
        MerkleTree{num_leaf: data_len, tree_size: v.len(), nodes_array: v, level_num: l}

    }

    pub fn root(&self) -> H256 {
        //unimplemented!()
        self.nodes_array[self.tree_size - 1]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        //unimplemented!()
        let depth = self.level_num.len();
        let mut v: Vec<H256> = Vec::new();
        let mut level_idx = index;
        let mut sibling = 0;
        let mut level = 0;
        let mut level_base = 0;
        while level < depth - 1 {
            if level_idx % 2 == 0 {
                sibling = level_idx + 1;
            } else {
                sibling = level_idx - 1;
            }
            v.push(self.nodes_array[sibling + level_base]);
            level_base += self.level_num[level];
            level += 1;
            level_idx /= 2;
        }
        return v;
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    //unimplemented!()
    let n = proof.len();
    //let zero_hash: [u8; 32] = [0;32];
    let mut next_hash: H256 = *datum;
    let mut cur_idx = index;
    for i in 0..n {
        let mut raw_hash: [u8; 64] = [0; 64];
        if cur_idx % 2 == 0 {
            raw_hash[0..32].copy_from_slice(next_hash.as_ref());
            raw_hash[32..64].copy_from_slice(proof[i].as_ref());
        } else {
            raw_hash[0..32].copy_from_slice(proof[i].as_ref());
            raw_hash[32..64].copy_from_slice(next_hash.as_ref());
        }
        next_hash = ring::digest::digest(&ring::digest::SHA256, &raw_hash).into();
        cur_idx /= 2;
    } 
    next_hash == *root
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}
