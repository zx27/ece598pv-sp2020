use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use bincode;


#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
    coinid: String,
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    //unimplemented!()
    let encoded_trans: Vec<u8> = bincode::serialize(t).unwrap();
    let encoded_trans2: &[u8] = &encoded_trans;
    key.sign(encoded_trans2)
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    //unimplemented!()
    let encoded_trans: Vec<u8> = bincode::serialize(t).unwrap();
    let encoded_trans2: &[u8] = &encoded_trans;
    let peer_public_key = UnparsedPublicKey::new(&ED25519, public_key.as_ref());
    peer_public_key.verify(encoded_trans2, signature.as_ref()).is_ok()
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    use rand::{thread_rng, Rng};
    use rand::distributions::Alphanumeric;

    pub fn generate_random_transaction() -> Transaction {
        //Default::default()
        //unimplemented!()
        let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(10)
        .collect();
        Transaction{coinid: rand_string}
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
